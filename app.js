  //app.js
  var aldstat = require("./utils/ald-stat.js")
  App({
      onLaunch: function(options) {
          let pid = 0;
          console.log(options)
              // 显示当前页面的转发按钮
          wx.showShareMenu({
              // 是否使用带 shareTicket 的转发
              withShareTicket: true
          })
          if (options.query.scene) {
              this.globalData.pid = options.query.scene
              pid = options.scene || 0
                    console.log(options.query.scene)
          }

          if (options.query.account_id) {
              this.globalData.pid = options.query.account_id
              pid = options.query.account_id || 0
                    console.log(options.query.account_id)
          }

          let openId = wx.getStorageSync('openId') || ''
          let userinfo = wx.getStorageSync('userinfo') || ''
          let OPEND_ID_URL = 'https://rwan.org.cn/api/me/getopenid'
          if (openId) {} else {
              wx.navigateTo({
                  url: '/pages/sq/sq'
              })
          }
          if (options.scene == 1007 || options.scene == 1008) {
              this.globalData.share = true
          } else {
              this.globalData.share = false
          };
          wx.getSystemInfo({
              success: (res) => {
                  this.globalData.height = res.statusBarHeight
              }
          })
      },
      onShow: function(options) {
          console.log(options)
      },
      u: {
          H: 'https://rwan.org.cn',
          me: {
              GETOPENID: '/api/me/getopenid', //获取openid
              UPLOADIMG: '/api/me/uploadimg',
              OSUPLOADIMG: '/api/me/osuploadimg'

          },
          river: {
              STARTRIVER: '/api/river/startriver', //开始巡河
              ADDPMARK: '/api/river/addpmark', //开始巡河
              GETWXSTEPS: '/api/river/getwxsteps', //获取微信运动步数
              GETRIVER: '/api/river/getriver',
              INDEX: '/api/index/index', //首页
              ENDRIVER: '/api/river/endriver', //结束巡河
              GETMARKINFO: '/api/river/getmarkinfo', //获取标记点信息
              GETTAGS: '/api/river/tags', //获取标记点里面标签
              ADDMARK: '/api/river/addmark', //标记
              GETMARKLIST: '/api/river/getmarklist', //获取历史记录
              GETRIVERINFO: '/api/river/getriverinfo', //河段
              FOLLOW: '/api/follow/index', //关注接口
              GETINFO: '/api/river/getinfo',
              MEINFO: '/api/me/info',
              GETRIVERMARKLIST: '/api/river/getrivermarklist',
              ACTICLE: '/api/index/acticle',
              UPSINDEX: '/api/ups/index',
              CREATESHAREPNG: '/api/qrcode/createsharepng', //巡河海报
              COMMENTS: '/api/comments/index', //评论功能
              GRTUSERMARKLIST: '/api/river/getusermarklist', //获取用户标记点
              GETUSERINFO: '/api/me/getuserinfo', //我的
              GETINVITEUSER: '/api/me/getinviteuser', //邀请人数
              GETRIVERPAROL: '/api/river/getriverparol', //获取本次巡河
              INDEXNEW: '/api/Indexnew/index', //新首页
              GETWEATHER: '/api/Indexnew/getweather',
              UPLOADIMG: '/api/me/uploadimg', //上传图片
              CREATEGROUP: '/api/group/createGroup', //创建群组
              GROUPLIST: '/api/group/grouplist', //群组
              GROUPINFO: '/api/group/groupinfo', //群组信息
              GROUPJOIN: '/api/group/join', //加入群组
              EXITGROUP: '/api/group/exitgroup', //退出群组
              GETPORALLIST: '/api/me/getporallist', //巡护记录
              EDITGROUP: '/api/group/editGroup', //编辑群组
              GROUPRANK: '/api/group/grouprank', //z
              GETPORALLIST: '/api/me/getporallist', //巡护记录
              FEEDBACK: '/api/me/feedback', //留言反馈
              SHOWNEWMARKS: '/api/indexnew/shownewmarks', //首页热门下面加载更多新动态
              SHOWHOTMARKS: '/api/indexnew/showhotmarks', //首页热门下面加载更多新动态
              GETMOREMARKS: '/api/group/getmoremarks',
              FOLLOWMSG: '/api/me/followmsg', //我的关注
              GETMSG: '/api/me/getmsg', //我的消息
              GETUSERMLIST: '/api/river/getusermlist', //我的标记点
              GETNEWMSGNUM: '/api/me/getnewmsgnum', //未读消息
              UPDATEMSG: '/api/me/updatemsg', //修改为已读
              CHECKPOINTS: '/api/group/checkpoints',
              GETGROUPUSER: '/api/group/getgroupuser',
              UPDATEGROUPUSER: '/api/group/updategroupuser',
              USERGROUPINFO: '/api/group/usergroupinfo',
              GETLENGTH: '/api/river/getlength',
              GETCITY: '/api/indexnew/getcity',
              GETCOMMENT: '/api/river/getcomment',
              GETACCOUNT: '/api/indexnew/getaccount',
              GETGROUPID:'/api/group/getgroupid',
          }
      },
      globalData: {
          resBaseUrl: 'https://rwan.org.cn',
          share: false, // 分享默认为false
          height: 0,
          pid: 0,
          ossUrl: 'https://riverwatcher-bucket.oss-cn-shenzhen.aliyuncs.com/'
      }
  })