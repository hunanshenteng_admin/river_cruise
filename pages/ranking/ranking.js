// pages/ranking/ranking.js
var util = require("../../utils/util.js")
var api = require("../../utils/api.js")
let app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        nvabarData: {
            showCapsule: 1, //是否显示左上角图标   1表示显示    0表示不显示
            title: '排名', //导航栏 中间的标题
        },
        height: app.globalData.height * 2 + 20,
        type: 1,
        id: 0,
        content: {},
        name: '',
    },
    tableCheck: function(e) {
        var that = this
        that.setData({
            type: e.currentTarget.id
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        var that = this
        that.setData({
            id: options.id,
            type: options.type,
            name: options.name
        })
    },
    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {
        var that = this
        var data = {
            id: that.data.id
        }
        var url = app.u.H + app.u.river.GROUPRANK
        api.requestUrl(data, url).then(res => {
            if (res.code == 200) {
                that.setData({
                    content: res.data
                })
            }
        })
    },
    onShareAppMessage: function(res) {
        let that = this
        let uid = wx.getStorageSync('userinfo').id
        let nickname = wx.getStorageSync('userinfo').nick_name
            // 来自页面内转发按钮
        return {
            title: nickname + "本周巡 护排行榜，看看你排第几",
            path: "pages/ranking/ranking?account_id=" + uid + "&id=" + that.data.id
        }

    },
})