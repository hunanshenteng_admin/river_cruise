//index.js
//获取应用实例
const app = getApp()

Page({
    data: {
        userInfo: {},
        hasUserInfo: false,
        canIUse: wx.canIUse('button.open-type.getUserInfo')
    },
    //事件处理函数
    bindViewTap: function() {
        wx.navigateTo({
            url: '../logs/logs'
        })
    },
    onLoad: function() {

    },
    getUserInfo: function(e) {
        let OPEND_ID_URL = app.globalData.resBaseUrl + app.u.me.GETOPENID
        let pid = app.globalData.pid || 0
        wx.login({
            success(res) {
                wx.getUserInfo({
                    lang: "zh_CN",
                    success: function(e) {
                        console.log(e)
                        if (res.code) {
                            //发起网络请求
                            wx.request({
                                url: OPEND_ID_URL,
                                data: {
                                    code: res.code,
                                    nickName: e.userInfo.nickName,
                                    avatarUrl: e.userInfo.avatarUrl,
                                    gender: e.userInfo.gender,
                                    city: e.userInfo.city,
                                    province: e.userInfo.province,
                                    pid: pid
                                },
                                success: function(res) {
                                    //console.log(res)
                                    wx.setStorageSync('openId', res.data.data.openid)
                                    wx.setStorageSync('userinfo', res.data.data.userInfo)
                                    wx.navigateBack({
                                        delta: 1
                                    })
                                }
                            })
                        } else {

                        }
                    },
                    fail: function() {

                    }
                })

            }
        })
    }
})