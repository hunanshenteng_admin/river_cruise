// pages/addgroup/addgroup.js
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		type: 1,
	},
	create: function () {
		wx.navigateTo({
			url: '../create/create',
		})
	},
	groupof: function () {
		wx.navigateTo({
			url: '../groupof/groupof',
		})
	},
	tableCheck: function (e) {
		var that = this
		that.setData({
			type: e.currentTarget.dataset.id
		})
	},
	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

})