// pages/patrollist/patrollist.js
var util = require("../../utils/util.js")
var api = require("../../utils/api.js")
let app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    nvabarData: {
      showCapsule: 1, //是否显示左上角图标   1表示显示    0表示不显示
      title: '巡河详情', //导航栏 中间的标题
    },
    height: app.globalData.height * 2 + 20,
    uid: 0,
    id: 0,
    cover: '',
    nickname: '',
    polyline: [{
      points: [],
      color: '#FF0000DD',
      width: 2,
      dottedLine: true
    }],
    content: {},
    longitude: '',
    latitude: '',
    ups: 0,
    sharaType: 0,
  },
  filename: function() {
    var that = this
    wx.navigateTo({
      url: '../filename/filename?id=' + that.data.id,
    })
  },
  cancel: function() {
    wx.switchTab({
      url: '../rivercruise/rivercruise',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this
    that.setData({
      id: options.id,
      sharaType:options.sharaType == undefined ? that.data.sharaType : options.sharaType
    })
    wx.getStorage({
      key: 'userinfo',
      success: function(res) {
        that.setData({
          cover: res.data.cover,
          nickname: res.data.nick_name,
        })
      }
    })
    var that = this
    var data = {
      id: options.id,
      account_id: wx.getStorageSync('userinfo').id
    }
    var url = app.u.H + app.u.river.GETRIVERPAROL
    api.requestUrl(data, url).then(res => {
      if (res.code == 200) {
        var up = "polyline[0].points";
        that.setData({
          [up]: res.data.points,
          content: res.data,
          latitude: res.data.start_lat,
          longitude: res.data.start_long,
          ups: res.data.ups
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
  },
  comments: function() {
    var that = this

    wx.navigateTo({
      url: '../comments/comments?type=patrol&id=' + that.data.id
    })
  },
  ups: function(e) {
    var that = this
    var data = {
      type: 'patrol',
      id: that.data.id,
      account_id: wx.getStorageSync('userinfo').id
    }
    var url = app.u.H + app.u.river.UPSINDEX

    var dataups = 'content.ups'

    api.requestUrl(data, url).then(res => {
      if (res.code == 200) {
        api.showError(res.msg)
        that.setData({
          ups: that.data.ups + 1,
        })
      } else {
        api.showError(res.msg)
      }
    })
  },
  onShareAppMessage(res) {
    var that = this

    let uid = wx.getStorageSync('userinfo').id
    let nickname = wx.getStorageSync('userinfo').nick_name
    // 来自页面内转发按钮
    return {
      title: nickname + " 邀您一起巡河,河流因您而变",
      path: '/pages/patrollist/patrollist?account_id=' + uid + '&id=' + that.data.id + '&sharaType=1',
    //   imageUrl: '../res/wxshare.jpg',
    }
  }
})