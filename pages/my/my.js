// pages/my/my.js
var util = require("../../utils/util.js")
var api = require("../../utils/api.js")
let app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        cover: '',
        nickName: '',
        mobile: '',
        nvabarData: {
            showCapsule: 0, //是否显示左上角图标   1表示显示    0表示不显示
            title: '我的', //导航栏 中间的标题
        },
        height: app.globalData.height * 2 + 20,
        content: {},
        uid: 0,
        messageCount: 0,
    },
    mythemileage: function() {
        wx.navigateTo({
            url: '../mythemileage/mythemileage',
        })
    },
    myunit: function() {
        wx.navigateTo({
            url: '../myunit/myunit',
        })
    },
    filename: function() {
        wx.navigateTo({
            url: '../filename/filename',
        })
    },
    servernet: function() {
        wx.makePhoneCall({
            phoneNumber: '189-7485-9157',
        })
    },
    mybinding: function() {
        wx.navigateTo({
            url: '../mybinding/mybinding',
        })
    },
    messageFeedback: function() {
        wx.navigateTo({
            url: '../messageFeedback/messageFeedback',
        })
    },
    myMessage: function() {
        var that = this;
        if (that.data.messageCount > 0) {
            var data = {
                account_id: wx.getStorageSync('userinfo').id
            }
            var url = app.u.H + app.u.river.UPDATEMSG
            api.requestUrl(data, url).then(res => {
                if (res.code == 200) {
                    that.setData({
                        messageCount: 0,
                    })
                }
            })
        }
        // UPDATEMSG
        // id = msgid
        wx.navigateTo({
            url: '../myMessage/myMessage',
        })
    },
    myAttention: function() {
        wx.navigateTo({
            url: '../myAttention/myAttention',
        })
    },
    myinvitation: function() {
        wx.navigateTo({
            url: '../myinvitation/myinvitation',
        })
    },
    article: function() {
        wx.navigateTo({
            url: '../article/article?id=' + 118,
        })
    },
    showpoint: function() {

        wx.showToast({
            title: '积分功能尚未完善，正在开发中',
            icon: 'none',
            duration: 2000
        })
    },
    group: function() {
        var groupid = wx.getStorageSync("userinfo").group_id
        if (groupid == 0 || groupid == undefined || groupid == "") {
            setTimeout(function() {
                wx.switchTab({
                    url: '../group/group',
                })
                api.showError("你没有加入团队，请加入团队")
            }, 1000)
        } else {
            wx.navigateTo({
                url: '../dynamic/dynamic?id=' + groupid,
            })
        }
    },

    getMessegeCount() {
        var that = this;
        var data = {
            account_id: wx.getStorageSync('userinfo').id
        }
        var url = app.u.H + app.u.river.GETNEWMSGNUM
        api.requestUrl(data, url).then(res => {
            if (res.code == 200) {
                // that.setData({
                //     messageCount: res.data,
                // })
                wx.setStorageSync('message', res.data)
                that.setData({
                    messageCount: res.data
                })
            }
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */

    onLoad: function(options) {
        var that = this
        if (wx.getStorageSync('userinfo').id) {
            var data = {
                account_id: wx.getStorageSync('userinfo').id
            }
            var url = app.u.H + app.u.river.GETUSERINFO
            api.requestUrl(data, url).then(res => {
                if (res.code == 200) {
                    wx.setStorageSync("userinfo", res.data)
                    that.setData({
                        content: res.data,
                    })

                }
            })
        } else {

        }

    },
    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {
        var that = this
        wx.getStorage({
            key: 'userinfo',
            success: function(res) {
                that.setData({
                    cover: res.data.cover,
                    nickName: res.data.nick_name,
                    mobile: res.data.mobile,
                })
            }
        })
        that.getMessegeCount();

    },
    onShareAppMessage: function(res) {
        var that = this
        that.setData({
            uid: wx.getStorageSync('userinfo').id
        })
        let nickname = wx.getStorageSync('userinfo').nick_name
        if (res.from === 'button') {
            // 来自页面内转发按钮
        } else {
            return {
                // title: nickname + " 邀请你加入巡河宝，跟我一起去河边走走吧！",
                title: '点开就能知道，保护河流我是认真的',
                path: "pages/my/my?account_id=" + that.data.uid
            }
        }
    },

})