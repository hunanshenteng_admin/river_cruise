// pages/plat/plat.js
var util = require("../../utils/util.js")
var api = require("../../utils/api.js")
let app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        nvabarData: {
            showCapsule: 1, //是否显示左上角图标   1表示显示    0表示不显示
            title: '地图', //导航栏 中间的标题
        },
        height: app.globalData.height * 2 + 20,
		latitude:'',
		longitude:'',
		height2:'',
		markers: {},
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        console.log(options.latitude)
        console.log(options.longitude)
        var that = this
        if (options.latitude == undefined && options.longitude == undefined) {

        } else {
            that.setData({
                latitude: options.latitude,
				longitude: options.longitude, 
				markers: [{
					iconPath: '../res/guanzhu-004.png',
					id: 0,
					latitude: options.latitude,
					longitude: options.longitude,
					width: 18,
					height: 30
				}],
            })
        }
		wx.getSystemInfo({
			success: function (res) {
				that.setData({
					height2: res.screenHeight * 2,
				})
			},
		})
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },
})