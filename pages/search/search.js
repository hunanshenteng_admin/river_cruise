// pages/search/search.js
var util = require("../../utils/util.js")
var api = require("../../utils/api.js")
var QQMapWX = require('../../utils/qqmap/qqmap-wx-jssdk.js');
let app = getApp();
var qqmapsdk;
Page({

    /**
     * 页面的初始数据
     */
    data: {
        nvabarData: {
            showCapsule: 1, //是否显示左上角图标   1表示显示    0表示不显示
            title: '选择城市', //导航栏 中间的标题
        },
        height: app.globalData.height * 2 + 20,
        type: 1,
        name: '',
        content: {},
        province: '',
        city: '',
    },
    userinput: function(e) {
        var that = this
        that.setData({
            name: e.detail.value
        })
        var data = {
            name: e.detail.value
        }
        var url = app.u.H + app.u.river.GETCITY
        api.requestUrl(data, url).then(res => {
            console.log(res)

            if (res.code == 200) {
                that.setData({
                        type: 2,
                        content: res.data
                    })
                    // that.setData({
                    //     isfollow: res.data
                    // })
            }
        })
    },
    cancel: function() {
        wx.navigateBack({
            delta: 2
        })
    },
    cut: function() {
        var that = this

        console.log(111111111)
        var data = {
            name: that.data.name
        }
        var url = app.u.H + app.u.river.GETCITY
        api.requestUrl(data, url).then(res => {
            console.log(res)

            if (res.code == 200) {
                that.setData({
                        type: 2,
                        content: res.data
                    })
                    // that.setData({
                    //     isfollow: res.data
                    // })
            }
        })
    },
    jump: function(e) {
        var that = this

        var name = e.currentTarget.dataset.name
        var parent = e.currentTarget.dataset.parent
        if (parent == '' && name == '') {

        } else {
            wx.setStorageSync("usercity", name)
            wx.setStorageSync("userparent", parent)
            wx.setStorageSync("cityfresh", 1)
            qqmapsdk.geocoder({
                address: name, //用户输入的地址（注：地址中请包含城市名称，否则会影响解析效果），如：'北京市海淀区彩和坊路海淀西大街74号'
                complete: res => {
                    console.log(1111)
                    console.log(res.result.location); //经纬度对象
                    wx.setStorageSync('address', res.result.location)

                }
            })
            wx.navigateBack({
                delta: 2
            })
        }
    },
    getUserLocation: function() {
        let vm = this;
        wx.getSetting({
            success: (res) => {
                // res.authSetting['scope.userLocation'] == undefined    表示 初始化进入该页面
                // res.authSetting['scope.userLocation'] == false    表示 非初始化进入该页面,且未授权
                // res.authSetting['scope.userLocation'] == true    表示 地理位置授权
                if (res.authSetting['scope.userLocation'] != undefined && res.authSetting['scope.userLocation'] != true) {
                    wx.showModal({
                        title: '请求授权当前位置',
                        content: '需要获取您的地理位置，请确认授权',
                        success: function(res) {
                            if (res.cancel) {
                                wx.showToast({
                                    title: '拒绝授权',
                                    icon: 'none',
                                    duration: 1000
                                })
                            } else if (res.confirm) {
                                wx.openSetting({
                                    success: function(dataAu) {
                                        if (dataAu.authSetting["scope.userLocation"] == true) {
                                            wx.showToast({
                                                    title: '授权成功',
                                                    icon: 'success',
                                                    duration: 1000
                                                })
                                                //再次授权，调用wx.getLocation的API
                                            vm.getLocation();
                                        } else {
                                            wx.showToast({
                                                title: '授权失败',
                                                icon: 'none',
                                                duration: 1000
                                            })
                                        }
                                    }
                                })
                            }
                        }
                    })
                } else if (res.authSetting['scope.userLocation'] == undefined) {
                    //调用wx.getLocation的API
                    vm.getLocation();
                } else {
                    //调用wx.getLocation的API
                    vm.getLocation();
                }
            }
        })
    },
    // 微信获得经纬度
    getLocation: function() {
        let vm = this;
        wx.getLocation({
            type: 'wgs84',
            success: function(res) {
                var latitude = res.latitude
                var longitude = res.longitude
                var speed = res.speed
                var accuracy = res.accuracy;
                vm.getLocal(latitude, longitude)
            },
            fail: function(res) {}
        })
    },
    getLocal: function(latitude, longitude) {
        let vm = this;
        qqmapsdk.reverseGeocoder({
            location: {
                latitude: latitude,
                longitude: longitude
            },
            success: function(res) {
                let province = res.result.ad_info.province
                let city = res.result.ad_info.city

                wx.setStorageSync("usercity", city)
                wx.setStorageSync("userparent", province)
                wx.setStorageSync("cityfresh", 1)


                location = { "lng": longitude, "lat": latitude }
                wx.setStorageSync('address', location)


                wx.navigateBack({
                    delta: 2
                })
            },
            fail: function(res) {},
            complete: function(res) {
                // console.log(res);
            }
        });
    },
    obtain: function(e) {
        var that = this
        qqmapsdk = new QQMapWX({
            key: 'TTWBZ-6L56U-W55V3-BJZJ6-KGGYF-O2BRB'
        })
        wx.getLocation({
            type: 'gcj02',
            success: function(res) {
                that.getUserLocation()
                that.getLocal(res.laitude, res.longitude)
            },
        })

    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        var that = this
        qqmapsdk = new QQMapWX({
            key: 'TTWBZ-6L56U-W55V3-BJZJ6-KGGYF-O2BRB'
        })
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },

})