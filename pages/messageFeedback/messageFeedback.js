// pages/messageFeedback/messageFeedback.js
var util = require("../../utils/util.js")
var api = require("../../utils/api.js")
let app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        nvabarData: {
            showCapsule: 1, //是否显示左上角图标   1表示显示    0表示不显示
            title: '留言反馈', //导航栏 中间的标题
        },
        height: app.globalData.height * 2 + 20,
        nickname: ""
    },

    bindFormSubmit: function(e) {
        if (e.detail.value.textarea == "") {
            api.showError('留言不能为空')
            return;
        }
        var data = {
            account_id: wx.getStorageSync('userinfo').id,
            content: e.detail.value.textarea
        }
        var url = app.u.H + app.u.river.FEEDBACK
        api.requestUrl(data, url).then(res => {
            if (res.code == 200) {
                api.showError(res.msg)
                setTimeout(function() {

                    wx.switchTab({
                        url: '/pages/my/my'
                    })
                }, 1000)
            } else {
                api.showError(res.msg)
            }
        })
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {
        let that = this
        that.setData({
            nickname: wx.getStorageSync('userinfo').nick_name,
        })
    },

})