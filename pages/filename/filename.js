// pages/filename/filename.js
var util = require("../../utils/util.js")
var api = require("../../utils/api.js")
let app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        conten: {},
        nvabarData: {
            showCapsule: 1, //是否显示左上角图标   1表示显示    0表示不显示
            title: '环保成就', //导航栏 中间的标题
        },
        height: app.globalData.height * 2 + 20,
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        wx.showLoading({
            title: '生成海报中···'
        })
        var that = this
        var data = { river_id: options.id, account_id: wx.getStorageSync('userinfo').id }
        var url = app.u.H + app.u.river.CREATESHAREPNG
        api.requestUrl(data, url).then(res => {
            wx.hideLoading()
            if (res.code == 200) {
                that.setData({
                    conten: res.data
                })
            }
        })
    },
    save: function() {
        var that = this
        wx.downloadFile({
            url: that.data.conten,
            success: function(res) {
                wx.saveImageToPhotosAlbum({
                    filePath: res.tempFilePath,
                    success(result) {
						setTimeout(function () {
							api.showSuccess('保存成功，请到手机相册内查看')
						}, 800)
						wx.navigateBack({
							delta: 1
						})
                    }
                })

                // wx.saveFile({
                //     tempFilePath: res.tempFilePath,
                //     success: function(res) {
                //         console.log(res.savedFilePath)
                //     }
                // })

            }
        })

    },
    cancel: function() {
        wx.navigateBack({
            delta: 2
        })
    },
    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },

})