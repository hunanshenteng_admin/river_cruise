// pages/readetails/readetails.js
var util = require("../../utils/util.js")
var api = require("../../utils/api.js")
let app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        content: {},
        contentlist: {},
        isfollow: 0,
        id: 0,
        type: 0,
        nvabarData: {
            showCapsule: 1, //是否显示左上角图标   1表示显示    0表示不显示
            title: '河段信息', //导航栏 中间的标题
        },
        height: app.globalData.height * 2 + 20,
    },
    follow: function() {
        var that = this
        wx.getStorage({
            key: 'userinfo',
            success: function(res) {
                var data = {
                    account_id: res.data.id,
                    id: that.data.id,
                    type: that.data.type,
                }
                var url = app.u.H + app.u.river.FOLLOW
                api.requestUrl(data, url).then(res => {
                    if (res.code == 200) {
                        that.setData({
                            isfollow: res.data
                        })
                    }
                })
            },
        })

    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        var that = this
        that.setData({
            id: options.id,
            type: options.type,
        })
        var url = app.u.H + app.u.river.GETRIVERMARKLIST
        api.requestUrl(data, url).then(res => {
                if (res.code == 200) {
                    that.setData({
                        content: res.data
                    })
                }
            })
            //
        var data = { id: options.id, account_id: wx.getStorageSync('userinfo').id }
        var url = app.u.H + app.u.river.GETRIVERINFO
        api.requestUrl(data, url).then(res => {
            if (res.code == 200) {
                that.setData({
                    contentlist: res.data,
                    isfollow: res.data.followstate
                })
            }
        })
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },
})