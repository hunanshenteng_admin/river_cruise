// pages/myinvitation/myinvitation.js
var util = require("../../utils/util.js")
var api = require("../../utils/api.js")
let app = getApp();
var village_LBS = function(that) {
  //var that = this;
  // ------------ 腾讯LBS地图  --------------------
  wx.getLocation({
    type: 'gcj02', //返回可以用于wx.openLocation的经纬度
    success: function(res) {
      // 调用接口, 坐标转具体位置 -xxz0717
      that.setData({
        longitude: res.longitude,
        latitude: res.latitude,
      })
    }
  })
}
Page({

  /**
   * 页面的初始数据
   */
  data: {
    nvabarData: {
      showCapsule: 1, //是否显示左上角图标   1表示显示    0表示不显示
      title: '标记点数', //导航栏 中间的标题
    },
    height: app.globalData.height * 2 + 20,
    content: {},
    contentlength:0,
    isShow: 1,
    markers: [],
    longitude: '',
    latitude: '',
  },
  //获取id实现臭水和正常水的切换功能
  choose: function(e) {
    var that = this
    that.setData({
      isShow: e.currentTarget.dataset.id,
    });
    that.getSign();
  },
  cruisedetails: function(e) {
    var that = this
    wx.navigateTo({
      url: '../cruisedetails/cruisedetails?id=' + e.currentTarget.dataset.id + '&type=mark',
    })
  },
  getSign() {
    var that = this;
    var data = {
      account_id: wx.getStorageSync('userinfo').id,
      state: that.data.isShow,
    };
    var url = app.u.H + app.u.river.GETUSERMLIST;
    api.requestUrl(data, url).then(res => {
      if (res.code == 200) {
        that.setData({
          markers: res.data.marks
        })
      }
    });
  },
  getList() {
    var that = this;
    var data = {
      account_id: wx.getStorageSync('userinfo').id,
    };
    var url = app.u.H + app.u.river.GETUSERMLIST;
    api.requestUrl(data, url).then(res => {
      if (res.code == 200) {
        that.setData({
          content: res.data.list.map(item=>{
            if (item.photos.length>0){
              if (item.photos[0]!=""){
                item.img = item.photos[0];
              }else{
                item.img = item.cover;
              }
            }else{
              item.img = item.cover;
            }
            
            return item;
          }),
          contentlength: res.data.list.length,
        })
      }
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    let that = this;
    this.getList();
    this.getSign();
    village_LBS(that);
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
  },

})