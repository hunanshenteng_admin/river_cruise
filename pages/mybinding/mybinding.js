// pages/mybinding/mybinding.js
var util = require("../../utils/util.js")
var api = require("../../utils/api.js")
let app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        mobile: '',
        cover: '',
        nickName: '',
        gender: 0,
        intro: '',
        userid: 0,
        nvabarData: {
            showCapsule: 1, //是否显示左上角图标   1表示显示    0表示不显示
            title: '绑定手机号', //导航栏 中间的标题
        },
        height: app.globalData.height * 2 + 20,
    },
    mobileinput: function(e) {
        var that = this
        that.setData({
            mobile: e.detail.value,
        })
    },
    boutt: function() {
        var that = this
		var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1})|(17[0-9]{1})|(19[0-9]{1})|(14[0-9]{1}))+\d{8})$/; // 判断手机号码的正则
        if (that.data.mobile.length == 0) {
            api.showError('手机号码不能为空')
            return;
        }

        if (that.data.mobile.length < 11) {
            api.showError('手机号码长度有误！')
            return;
        }

        if (!myreg.test(that.data.mobile)) {
            api.showError('错误的手机号码！')
            return;
        }
        var data = {
            uid: that.data.userid,
            nick_name: that.data.nickName,
            gender: that.data.gender,
            intro: that.data.intro,
            cover: that.data.cover,
            mobile: that.data.mobile,
        }
        var url = app.u.H + app.u.river.MEINFO
        api.requestUrl(data, url).then(res => {
            if (res.code == 200) {
                api.showError('绑定成功')
                wx.setStorageSync('userinfo', res.data)
                setTimeout(function() {
                    wx.navigateBack({
                        delta: 2
                    })
                }, 800)
            } else {
                api.showError(res.msg)
                wx.setStorageSync('userinfo', res.data)
            }
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        var that = this
        wx.getStorage({
            key: 'userinfo',
            success: function(res) {
                that.setData({
                    cover: res.data.cover,
                    nickName: res.data.nick_name,
                    gender: res.data.gender,
                    userid: res.data.id,
					mobile:res.data.mobile
                })
            }
        })
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },
})