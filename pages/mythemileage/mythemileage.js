// pages/mythemileage/mythemileage.js
var util = require("../../utils/util.js")
var api = require("../../utils/api.js")
let app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    nvabarData: {
      showCapsule: 1, //是否显示左上角图标   1表示显示    0表示不显示
      title: '巡河里程', //导航栏 中间的标题
    },
    height: app.globalData.height * 2 + 20,
    mileage: 0, //里程
    list: [],
    userImg: wx.getStorageSync('userinfo').cover,
  },
  terminate(e) {
    wx.navigateTo({
      url: '../patrollist/patrollist?id=' + e.currentTarget.dataset.id,
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this
    var data = {
      account_id: wx.getStorageSync('userinfo').id
    }
    var url = app.u.H + app.u.river.GETPORALLIST
    api.requestUrl(data, url).then(res => {
      if (res.code == 200) {
        that.setData({
          mileage: res.data.length,
          list: res.data.list,
          userImg: wx.getStorageSync('userinfo').cover
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

})