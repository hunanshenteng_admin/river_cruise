// pages/rivertourlist/rivertourlist.js
var util = require("../../utils/util.js")
var api = require("../../utils/api.js")
var QQMapWX = require('../../utils/qqmap/qqmap-wx-jssdk.js');
var qqmapsdk;
let app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        content: {},
        address: '',
        province: '',
        city: '',
        district: '',
        type: '',
        accountid: 0,
        id: 0,
        uid: 0,
        isfollow: 0,
        markid: 0,
        ups: 0,
        contentid: 0,
        cover: '',
        nvabarData: {
            showCapsule: 1, //是否显示左上角图标   1表示显示    0表示不显示
            title: '巡河列表', //导航栏 中间的标题
        },
        height: app.globalData.height * 2 + 20,
        photos: '',
    },
    comments: function(e) {
        wx.navigateTo({
            url: '../comments/comments?id=' + e.currentTarget.id,
        })
    },
    picture: function(e) {
        var that = this
        wx.navigateTo({
            url: '../picture/picture?id=' + e.currentTarget.dataset.id + '&type=' + that.data.type,
        })
    },
    follow: function() {
        var that = this
        wx.getStorage({
            key: 'userinfo',
            success: function(res) {
                var data = {
                    account_id: res.data.id,
                    id: that.data.markid,
                    type: that.data.type,
                }
                var url = app.u.H + app.u.river.FOLLOW
                api.requestUrl(data, url).then(res => {
                    if (res.code == 200) {
                        that.setData({
                            isfollow: res.data
                        })
                    }
                })
            },
        })

    },
    give: function(e) {
        var that = this
        var data = { type: that.data.type, id: e.currentTarget.id, account_id: wx.getStorageSync('userinfo').id }
        var url = app.u.H + app.u.river.UPSINDEX

        var dataups = 'content[' + e.currentTarget.dataset.index + '].ups'

        api.requestUrl(data, url).then(res => {
            if (res.code == 200) {
                var ups = that.data.ups
                that.setData({
                    [dataups]: ups + 1
                })
            } else {
                api.showError(res.msg)
            }
        })
    },
    cruisedetails: function(e) {
        var that = this
        wx.navigateTo({
            url: '../cruisedetails/cruisedetails?id=' + e.currentTarget.dataset.id + '&type=' + that.data.type,
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {



    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {
        var that = this
        qqmapsdk = new QQMapWX({
            key: 'TTWBZ-6L56U-W55V3-BJZJ6-KGGYF-O2BRB' // 必填
        });
        var data = {}
        var url = 'https://wntest.stw88.com/api/me/getlist'
        api.requestUrl(data, url).then(res => {
            if (res.code == 200) {
                for (let i = 0; i < res.data.length; i++) {
                    qqmapsdk.reverseGeocoder({
                        location: {
                            latitude: res.data[i].start_lat,
                            longitude: res.data[i].start_long,
                        },
                        success: function(r) {
                            startaddress = r.result.address
                            qqmapsdk.reverseGeocoder({
                                location: {
                                    latitude: res.data[i].end_lat,
                                    longitude: res.data[i].end_long,
                                },
                                success: function(e) {
                                    let data = {
                                        endaddress: e.result.address,
                                        startaddress: startaddress,
                                        id: res.data[$i].id
                                    }
                                    var url = 'https://wntest.stw88.com/api/me/updateriver'
                                    api.requestUrl(data, url).then(res => {})
                                }
                            });

                        }
                    });

                    var data = {}
                }
                that.setData({
                    content: res.data,
                })
            }
        })
    },
    cruisedetails2: function(e) {
        var that = this
        var src = e.currentTarget.dataset.src; //获取data-src
        var imgList = e.currentTarget.dataset.list; //获取data-list
        //图片预览
        wx.previewImage({
            current: src, // 当前显示图片的http链接
            urls: imgList // 需要预览的图片http链接列表
        })
    },
    onShareAppMessage(res) {
        var that = this
        that.setData({
            uid: wx.getStorageSync('userinfo').id
        })
        if (res.from === 'button') {
        }
        return {
            title: '巡河列表',
            path: '/pages/rivertourlist/rivertourlist?account_id=' + that.data.uid + '&type=' + that.data.type + '&markid=' + that.data.markid + '&id=' + that.data.id
        }
    }
})