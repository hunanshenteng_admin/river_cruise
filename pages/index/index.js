//index.js
var util = require("../../utils/util.js")
var api = require("../../utils/api.js")
let app = getApp();
var village_LBS = function(that) {
    //var that = this;
    // ------------ 腾讯LBS地图  --------------------
    wx.getLocation({
        type: 'gcj02', //返回可以用于wx.openLocation的经纬度
        success: function(res) {
            // 调用接口, 坐标转具体位置 -xxz0717
            that.setData({
                longitude: res.longitude,
                latitude: res.latitude,
            })
            var data = {
                long: res.longitude,
                lat: res.latitude,
                state: that.data.choushui
            }
            var url = app.u.H + app.u.river.INDEX
            api.requestUrl(data, url).then(res => {
                if (res.code == 200) {
                    that.setData({
                        markers: res.data
                    })
                }
            })
        }
    })
}
Page({
    data: {
        width: '',
        width2: '',
        width3: '',
        height2: '',
        height3: '',
        height4: '',
        longitude: '',
        latitude: '',
        markers: {},
        choushui: 0,
        ishow: 0,
        rigger: {},
        content: 1,
        userid: '',
        scale: 12,
        uid: 0,
        nvabarData: {
            showCapsule: 1, //是否显示左上角图标   1表示显示    0表示不显示
            title: '标记地图', //导航栏 中间的标题
        },
        // 此页面 页面内容距最顶部的距离
        height: app.globalData.height * 2 + 20,
    },
    markertap: function(e) {
        let id = e.markerId
        let data = {
            id: e.markerId
        }
        let url = app.u.H + app.u.river.GETMARKINFO
        api.requestUrl(data, url).then(res => {})
    },
    rivertourlist: function(e) {
        var that = this
        if (that.data.type == 3) {
            wx.navigateTo({
                url: '../reach/reach?id=' + e.currentTarget.id + '&type=river',
            })
        } else if (that.data.type == 1 || that.data.type == 2) {
            wx.navigateTo({
                url: '../rivertourlist/rivertourlist?id=' + e.currentTarget.id + '&type=mark&markid=' + e.currentTarget.dataset.markid,
            })
        }
    },
    regionchange(e) {
        // 地图发生变化的时候，获取中间点，也就是用户选择的位置toFixed
        if (e.type == 'end' && (e.causedBy == 'scale' || e.causedBy == 'drag')) {
            var that = this;
            this.mapCtx = wx.createMapContext("myMap");
            this.mapCtx.getCenterLocation({
                type: 'gcj02',
                success: function(res) {
                    that.setData({
                        latitude: res.latitude,
                        longitude: res.longitude,
                        circles: [{
                            latitude: res.latitude,
                            longitude: res.longitude,
                            color: '#FF0000DD',
                            fillColor: '#d1edff88',
                            radius: 3000, //定位点半径
                            strokeWidth: 1
                        }]
                    })
                }
            })
            // let that = this
            // this.mapCtx.getScale({
            //     success: function(e) {
            //         console.log(e.scale)
            //         if (e.scale < 12 && e.scale != that.data.scale) {
            //             for (let i = 0; i < that.data.markers.length; i++) {
            //                 let height = "markers[" + i + "].height"
            //                 let width = "markers[" + i + "].width"

            //                 that.setData({
            //                     [height]: 2.5 * e.scale + "rpx",
            //                     [width]: 1.7 * e.scale + "rpx",
            //                     scale: e.scale
            //                 })
            //             }
            //         }
            //     }
            // })
        }



    },
    controltap: function(e) {},
    thetrigger: function(e) {
        // that.mapCtx.getCenterLocation({
        // 	success: function (res) {
        // 		console.log(res.longitude)
        // 		console.log(res.latitude)
        // 	}
        // })
        // that.mapCtx.moveToLocation()
        var that = this
        var data = {
            id: e.markerId,
            account_id: wx.getStorageSync('userinfo').id
        }
        var url = app.u.H + app.u.river.GETINFO
        api.requestUrl(data, url).then(res => {
            if (res.code == 200) {
                that.setData({
                    rigger: res.data,
                    type: res.data.type,
                    content: 0,
                    longitude: res.data.longitude,
                    latitude: res.data.latitude,
                })


            }
        })
    },
    markpatrol: function(e) {
        wx.navigateTo({
            url: '../patrol/patrol?rpid=' + e.currentTarget.id,
        })
    },
    riverpatrol: function(e) {
        wx.navigateTo({
            url: '../patrol/patrol?pid=' + e.currentTarget.id,
        })
    },
    patrol: function(e) {
        wx.navigateTo({
            url: '../patrol/patrol?pid=' + e.currentTarget.id,
        })
    },
    mypatrol: function() {
        var that = this
        wx.getLocation({
            type: 'gcj02',
            success: function(res) {
                that.setData({
                    longitude: res.longitude,
                    latitude: res.latitude,
                })
                var data = {
                    long: res.longitude,
                    lat: res.latitude,
                    state: that.data.choushui,
                    account_id: wx.getStorageSync('userinfo').id
                }
                var url = app.u.H + app.u.river.INDEX
                api.requestUrl(data, url).then(res => {
                    if (res.code == 200) {
                        that.setData({
                            markers: res.data
                        })
                    }
                })
            },
        })
    },
    //详情页面
    cruisedetails: function(e) {
        var that = this
        if (that.data.type == 3) {
            wx.navigateTo({
                url: '../readetails/readetails?id=' + e.currentTarget.id + '&type=river',
            })
        } else if (that.data.type == 1 || that.data.type == 2) {
            wx.navigateTo({
                url: '../cruisedetails/cruisedetails?id=' + e.currentTarget.id + '&type=mark',
            })
        }
    },
    article: function(e) {
        wx.navigateTo({
            url: '../article/article?id=' + e.currentTarget.id,
        })
    },
    close: function() {
        this.setData({
            content: 1
        })
    },
    //获取id实现臭水和正常水的切换功能
    choose: function(e) {
        var that = this
        that.setData({
            choushui: e.currentTarget.dataset.id,
            content: 1

        })
        var that = this
      if (wx.getStorageSync('address') == '') {
        wx.getLocation({
            type: 'gcj02',
            success: function(res) {
                var lo = res.longitude
                var la = res.latitude
                var data = {
                    long: lo,
                    lat: la,
                    state: that.data.choushui,
                    account_id: 0,
                }
                var url = app.u.H + app.u.river.INDEX
                api.requestUrl(data, url).then(res => {
                    if (res.code == 200) {
                        that.setData({
                            longitude: lo,
                            latitude: la,
                            markers: res.data
                        })
                    }
                })
            },
        })
      }else{
        var data = {
          long: wx.getStorageSync('address').lng,
          lat: wx.getStorageSync('address').lat,
          state: that.data.choushui,
          account_id: 0,
        }
        var url = app.u.H + app.u.river.INDEX
        api.requestUrl(data, url).then(res => {
          if (res.code == 200) {
            that.setData({
              longitude: wx.getStorageSync('address').lng,
              latitude: wx.getStorageSync('address').lat,
              markers: res.data
            })
          }
        })
      }
    },
    gain: function() {},
    onLoad: function(options) {
        let state = options.state || 0
        var that = this
        wx.getSystemInfo({
            success: function(res) {
                that.setData({
                    choushui: state,
                    width: res.screenWidth * 2,
                    height2: res.screenHeight * 2 - app.globalData.height,
                    width2: res.screenWidth - res.screenWidth * 2 / 750 * 255,
                    topheight: res.screenHeight + (app.globalData.height * 2) + res.screenHeight * 2 / 1335 * 80,
                    width3: res.screenWidth - res.screenWidth * 2 / 750 * 215,
                })
            },
        })
        wx.getStorage({
            key: 'userinfo',
            success: function(res) {
                that.setData({
                    userid: res.data.id
                })
            },
        })
        wx.getLocation({
            type: 'gcj02',
            success: function(res) {

              if (wx.getStorageSync('address') == '' ) {
                that.setData({
                  longitude: res.longitude,
                  latitude: res.latitude,
                })
                var data = {
                  long: res.longitude,
                  lat: res.latitude,
                  state: state
                }
                var url = app.u.H + app.u.river.INDEX
                api.requestUrl(data, url).then(res => {
                  if (res.code == 200) {
                    that.setData({
                      markers: res.data
                    })
                  }
                })
              } else {
                that.setData({
                  latitude: wx.getStorageSync('address').lat,
                  longitude: wx.getStorageSync('address').lng
                })
                var data = {
                  long: wx.getStorageSync('address').lng,
                  lat: wx.getStorageSync('address').lat,
                  state: state
                }
                var url = app.u.H + app.u.river.INDEX
                api.requestUrl(data, url).then(res => {
                  if (res.code == 200) {
                    that.setData({
                      markers: res.data
                    })
                  }
                })
              }


            },
            fail: function(e) {
                wx.getSetting({
                    success: (res) => {
                        if (res.authSetting['scope.userLocation'] != undefined && res.authSetting['scope.userLocation'] != true) { //非初始化进入该页面,且未授权
                            wx.showModal({
                                title: '是否授权当前位置',
                                content: '需要获取您的地理位置，请确认授权，否则地图功能将无法使用',
                                success: function(res) {
                                    if (res.cancel) {} else if (res.confirm) {
                                        //village_LBS(that);
                                        wx.openSetting({
                                            success: function(data) {
                                                if (data.authSetting["scope.userLocation"] == true) {
                                                    wx.showToast({
                                                        title: '授权成功',
                                                        icon: 'success',
                                                        duration: 1000
                                                    })
                                                    //再次授权，调用getLocationt的API
                                                    village_LBS(that);
                                                } else {
                                                    wx.showToast({
                                                        title: '授权失败',
                                                        icon: 'success',
                                                        duration: 1000
                                                    })
                                                }
                                            }
                                        })
                                    }
                                }
                            })
                        } else if (res.authSetting['scope.userLocation'] == undefined) { //初始化进入
                            village_LBS(that);
                        }
                    }
                })
            }
        })
    },
    onShow: function() {
        var that = this
        wx.getLocation({
            type: 'gcj02',
            success: function(res) {},
            fail: function(e) {
                wx.getSetting({
                    success: (res) => {
                        if (res.authSetting['scope.userLocation'] != undefined && res.authSetting['scope.userLocation'] != true) { //非初始化进入该页面,且未授权
                            wx.showModal({
                                title: '是否授权当前位置',
                                content: '需要获取您的地理位置，请确认授权，否则地图功能将无法使用',
                                success: function(res) {
                                    if (res.cancel) {} else if (res.confirm) {
                                        //village_LBS(that);
                                        wx.openSetting({
                                            success: function(data) {
                                                if (data.authSetting["scope.userLocation"] == true) {
                                                    wx.showToast({
                                                        title: '授权成功',
                                                        icon: 'success',
                                                        duration: 1000
                                                    })
                                                    //再次授权，调用getLocationt的API
                                                    village_LBS(that);
                                                } else {
                                                    wx.showToast({
                                                        title: '授权失败',
                                                        icon: 'success',
                                                        duration: 1000
                                                    })
                                                }
                                            }
                                        })
                                    }
                                }
                            })
                        } else if (res.authSetting['scope.userLocation'] == undefined) { //初始化进入
                            village_LBS(that);
                        }
                    }
                })
            }
        })


    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {
        this.mapCtx = wx.createMapContext('myMap')

    },
    onShareAppMessage: function(res) {

        var that = this
        that.setData({
            uid: wx.getStorageSync('userinfo').id
        })
        if (res.from === 'button') {
            // 来自页面内转发按钮
        } else {

        }
        return {
            title: "巡河宝",
            path: "pages/index/index?account_id=" + that.data.uid
        }
    },


})