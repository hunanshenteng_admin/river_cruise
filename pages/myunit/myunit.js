// pages/myunit/myunit.js
var util = require("../../utils/util.js")
var api = require("../../utils/api.js")
let app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
		nvabarData: {
			showCapsule: 1, //是否显示左上角图标   1表示显示    0表示不显示
			title: '邀请人数', //导航栏 中间的标题
		},
		height: app.globalData.height * 2 + 20,
        content: {},
        contentlength:'',
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        var that = this
        var data = {
            account_id: wx.getStorageSync('userinfo').id
        }
		var url = app.u.H + app.u.river.GETINVITEUSER
        api.requestUrl(data, url).then(res => {
            if (res.code == 200) {
                that.setData({
                    content: res.data,
					contentlength: res.data.length
                })
            }else{
				that.setData({
					contentlength: res.data.length
				})
			}
        })
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },

})