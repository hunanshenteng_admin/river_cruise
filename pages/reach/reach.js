// pages/reach/reach.js
var util = require("../../utils/util.js")
var api = require("../../utils/api.js")
let app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        content: {},
        contentlength: 0,
        contentlist: {},
        id: 0,
        uid: 0,
        isfollow: 0,
        type: '',
        ups: 0,
        contlength: 0,
        cover: '',
        nvabarData: {
            showCapsule: 1, //是否显示左上角图标   1表示显示    0表示不显示
            title: '河段信息', //导航栏 中间的标题
        },
        height: app.globalData.height * 2 + 20,
    },
    follow: function() {
        var that = this
        wx.getStorage({
            key: 'userinfo',
            success: function(res) {
                var data = {
                    account_id: res.data.id,
                    id: that.data.id,
                    type: that.data.type,
                }
                var url = app.u.H + app.u.river.FOLLOW
                api.requestUrl(data, url).then(res => {
                    if (res.code == 200) {
                        that.setData({
                            isfollow: res.data
                        })
                    }
                })
            },
        })

    },
    give: function(e) {
        var that = this
        var data = {
            type: that.data.type,
            id: e.currentTarget.id,
            account_id: wx.getStorageSync('userinfo').id
        }
        var url = app.u.H + app.u.river.UPSINDEX

        var dataups = 'content[' + e.currentTarget.dataset.index + '].ups'

        api.requestUrl(data, url).then(res => {
            if (res.code == 200) {
                var ups = e.currentTarget.dataset.ups
                that.setData({
                    [dataups]: ups + 1
                })
            } else {
                api.showError(res.msg)
            }
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        var that = this
        that.setData({
            id: options.id,
            type: options.type,
        })
        wx.getStorage({
                key: 'userinfo',
                success: function(res) {
                    that.setData({
                        cover: res.data.cover,
                    })
                }
            })
            //
        var data = {
            id: options.id,
            account_id: wx.getStorageSync('userinfo').id
        }
        var url = app.u.H + app.u.river.GETRIVERINFO
        api.requestUrl(data, url).then(res => {
            if (res.code == 200) {
                that.setData({
                    contentlist: res.data,
                    isfollow: res.data.followstate
                })
            }
        })
    },
    picture: function(e) {
        var that = this
        wx.navigateTo({
            url: '../picture/picture?id=' + e.currentTarget.dataset.id,
        })
    },
    comments: function(e) {
        wx.navigateTo({
            url: '../comments/comments?id=' + e.currentTarget.id,
        })
    },
    picture2: function(e) {
        var src = e.currentTarget.dataset.src; //获取data-src
        var imgList = e.currentTarget.dataset.list; //获取data-list
        //图片预览
        wx.previewImage({
            current: src, // 当前显示图片的http链接
            urls: imgList // 需要预览的图片http链接列表
        })
    },
    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {
        var that = this
        var data = {
            pid: that.data.id
        }
        var url = app.u.H + app.u.river.GETRIVERMARKLIST
        api.requestUrl(data, url).then(res => {
            if (res.code == 200) {
                that.setData({
                    content: res.data,
                    contentlength: res.data.length,
                    ups: res.data.ups,
                    contlength: res.data.length,
                })
            }
        })
    },
    onShareAppMessage(res) {
        var that = this

        let uid = wx.getStorageSync('userinfo').id
        let nickname = wx.getStorageSync('userinfo').nick_name
            // 来自页面内转发按钮
        return {
            title: nickname + " 邀您一起巡河,河流因您而变",
            path: '/pages/reach/reach?account_id=' + uid + '&id=' + that.data.id,
            imageUrl: '../res/wxshare.jpg',
        }
    }
})