// pages/myAttention/myAttention.js
var util = require("../../utils/util.js")
var api = require("../../utils/api.js")
let app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    nvabarData: {
      showCapsule: 1, //是否显示左上角图标   1表示显示    0表示不显示
      title: '关注', //导航栏 中间的标题
    },
    height: app.globalData.height * 2 + 20,
    list: [],
    list1: [],
    type: 1,
    pages: 1,
    valueType: 'mark',
  },
  tableCheck: function(e) {
    var that = this;
    let id = e.currentTarget.id;
    let type = 'mark';
    if (id == 1) {
      type = 'mark';
    } else if (id == 2) {
      type = 'river';
    }
    that.setData({
      type: id,
      valueType: type
    })
    that.getList(type);
  },
  cruisedetails: function (e) {
    var that = this
    wx.navigateTo({
      url: '../cruisedetails/cruisedetails?id=' + e.currentTarget.dataset.id+'&type=mark',
    })
  },
	particulars: function (e) {
		wx.navigateTo({
			url: '../reach/reach?id=' + e.currentTarget.id + '&type=river',
		})
	},
  cruisedetails2: function (e) {
    var that = this
    var src = e.currentTarget.dataset.src; //获取data-src
    var imgList = e.currentTarget.dataset.list; //获取data-list
    //图片预览
    wx.previewImage({
      current: src, // 当前显示图片的http链接
      urls: imgList // 需要预览的图片http链接列表
    })
  },
  getList(e) {
    let that = this;
    var data = {
      account_id: wx.getStorageSync('userinfo').id,
      // account_id: 294,
      type: e,
      pages: 1,
    }
    var url = app.u.H + app.u.river.FOLLOWMSG
    api.requestUrl(data, url).then(res => {
        if (e == "mark") {
          that.setData({
            list: res.data,
            pages:1,
          })
        } else if (e == "river") {
          that.setData({
            list1: res.data,
            pages: 1,
          })
        }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.getList("mark");
    this.getList("river");
  },


  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    let that = this;
    var data = {
      account_id: wx.getStorageSync('userinfo').id,
      // account_id: 294,
      type: that.data.valueType,
      pages: that.data.pages + 1,
    }
    var url = app.u.H + app.u.river.FOLLOWMSG
    api.requestUrl(data, url).then(res => {
      if (that.data.valueType == "mark") {
        that.setData({
          list: that.data.list.concat(res.data),
          pages: that.data.pages + 1,
        })
      } else if (that.data.valueType == "river") {
        that.setData({
          list1: that.data.list1.concat(res.data),
          pages: that.data.pages + 1,
        })
      }
    })
  },
})