// pages/groupof/groupof.js
var util = require("../../utils/util.js")
var api = require("../../utils/api.js")
let app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        nvabarData: {
            showCapsule: 1, //是否显示左上角图标   1表示显示    0表示不显示
            title: '团队介绍', //导航栏 中间的标题
        },
        // 此页面 页面内容距最顶部的距离
        height: app.globalData.height * 2 + 20,
        content: {
          'pic': "http://riverwatcher-bucket.oss-cn-shenzhen.aliyuncs.com/upload/400465d263665bc9/57096a2995d16903.jpg"
        },
        id: 0,
        userid: 0,
        contentjojn: 0,
        userInfo: {
            content: 1
        },
        account_id: 0,
        address: '',
        intro: '',
        name: '',
        pic: '',
        hotmarks: {},
    },
    paimin: function(e) {
        var that = this
        wx.navigateTo({
            url: '../ranking/ranking?type=1&id=' + e.currentTarget.dataset.id,
        })
    },
    cruisedetails2: function(e) {
        var that = this
        var src = e.currentTarget.dataset.src; //获取data-src
        var imgList = e.currentTarget.dataset.list; //获取data-list
        //图片预览
        wx.previewImage({
            current: src, // 当前显示图片的http链接
            urls: imgList // 需要预览的图片http链接列表
        })
    },
    cruisedetails3: function(e) {
        var that = this
        var src = e.currentTarget.dataset.src; //获取data-src
        var imgList = e.currentTarget.dataset.list; //获取data-list
        //图片预览
        wx.previewImage({
            current: src, // 当前显示图片的http链接
            urls: [imgList] // 需要预览的图片http链接列表
        })
    },
    comments: function(e) {
        wx.navigateTo({
            url: '../comments/comments?id=' + e.currentTarget.id,
        })
    },
    // bjoin:function(e){
    // 	var that=this
    //     wx.navigateTo({
    // 		url: '../grfeditor/grfeditor?id=' + e.currentTarget.dataset.id + '&address=' + that.data.address + '&intro=' + that.data.intro + '&name=' + that.data.name + '&pic=' + that.data.pic,
    // 	})
    // },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        var that = this
        that.setData({
            id: options.id
        })

    },
    ujoin: function(e) {
        var that = this
        that.setData({
            userid: e.currentTarget.dataset.id,
        })
        var data = {
            id: that.data.userid,
            account_id: wx.getStorageSync('userinfo').id
        }
        var url = app.u.H + app.u.river.GROUPJOIN
        api.requestUrl(data, url).then(res => {
            if (res.code == 200) {
                wx.setStorageSync('userinfo', res.data)
                wx.setStorageSync('userInfo', that.data.userInfo)
                    // wx.navigateTo({
                    //     url: '../dynamic/dynamic?id=' + that.data.userid,
                    // })
                wx.redirectTo({
                    url: '../dynamic/dynamic?id=' + that.data.userid,
                })
                api.showError(res.msg)
            } else {
                api.showError(res.msg)
            }
        })
    },
    // //退出群组
    // tjoin: function(e) {
    //     var that = this
    //     that.setData({
    //         userid: e.currentTarget.dataset.id,
    //     })
    //     var data = {
    //         id: that.data.userid,
    //         account_id: wx.getStorageSync('userinfo').id
    //     }
    //     var url = app.u.H + app.u.river.EXITGROUP
    //     api.requestUrl(data, url).then(res => {
    // 		console.log(res.code)
    // 		wx.setStorageSync('userInfo', that.data.userInfo)
    //         if (res.code == 200) {

    //             // setTimeout(function() {
    //             //     api.showError(res.msg)
    //             //     wx.switchTab({
    //             //         url: '../newpage/newpage'
    //             //     })
    //             // }, 1000)
    //         } else {
    // 			setTimeout(function () {
    // 				api.showError(res.msg)
    // 				wx.switchTab({
    // 					url: '../group/group'
    // 				})
    // 			}, 1000)
    //             api.showError(res.msg)
    //         }
    //     })
    // },
    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {
        var that = this
        wx.getStorage({
            key: 'userinfo',
            success(res) {
                wx.showLoading({
                    "title":"群组数据加载中··",
                    "mask":true
                })
				let data = { account_id: res.data.id }
				console.log('account_id', res.data.id)
				let url = app.u.H + app.u.river.GETGROUPID
				api.requestUrl(data, url).then(res => {
					let group_id = res.data.join(',')+','
					console.log('group_Id',group_id)
					console.log('index', group_id.indexOf(that.data.id+','))
					if (group_id.indexOf(that.data.id+',') > -1) {
						wx.showLoading({
							title: '跳转已加入页面中··',
						})
						setTimeout(function () {
							getCurrentPages().pop()
							wx.hideLoading()
							wx.redirectTo({
// pages/dynamic/dynamic.js
								url: '/pages/dynamic/dynamic?id=' + that.data.id,
							})
						}, 1000)
            		}else{
						wx.getStorage({
							key: 'userinfo',
							success(res) {
								that.setData({
									account_id: res.data.id
								})
							}
						})
						var data = {
							id: that.data.id,
							account_id: wx.getStorageSync('userinfo').id
						}
						var url = app.u.H + app.u.river.GROUPINFO
						api.requestUrl(data, url).then(res => {
							if (res.code == 200) {
                                wx.hideLoading()
								that.setData({
									content: res.data,
									address: res.data.address,
									intro: res.data.intro,
									name: res.data.name,
									pic: res.data.pic,
									hotmarks: res.data.hotmarks,
								})
							}
						})
					}
				})
			}
        })
        
    },
    onShareAppMessage: function(res) {
        var that = this
        let uid = wx.getStorageSync('userinfo').id
        let nickname = wx.getStorageSync('userinfo').nick_name
        console.log(res)
        if (res.from === 'button') {
            // 来自页面内转发按钮
            if (that.data.hotmarks[res.target.dataset.index].photos) {
                return {
                    // title: nickname + " 邀您一起巡河,河流因您而变",
                    // title: '加入团队一起巡，环保路上不孤单',
                    title: that.data.hotmarks[res.target.dataset.index].intro,
                    path: "pages/cruisedetails/cruisedetails?account_id=" + uid + "&id=" + res.target.id + "&type=mark",
                    imageUrl: that.data.hotmarks[res.target.dataset.index].photos[0],
                    // imageUrl: '../res/wxshare.jpg',
                }
            } else {
                return {
                    // title: nickname + " 邀您一起巡河,河流因您而变",
                    // title: '加入团队一起巡，环保路上不孤单',
                    title: that.data.hotmarks[res.target.dataset.index].intro,
                    path: "pages/cruisedetails/cruisedetails?account_id=" + uid + "&id=" + res.target.id + "&type=mark",
                    // imageUrl: that.data.hotmarks[res.target.dataset.index].photos[0],
                    imageUrl: '../res/wxshare.jpg',
                }
            }

        } else {

            return {
                // title: nickname + " 邀您一起巡河,河流因您而变",
                title: '加入团队一起巡，环保路上不孤单',
                path: "pages/groupof/groupof?account_id=" + uid + "&id=" + that.data.id,
                // imageUrl: '../res/wxshare.jpg',
            }
        }
    },

})