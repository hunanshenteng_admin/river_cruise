// pages/cruisedetails/cruisedetails.js
var util = require("../../utils/util.js")
var api = require("../../utils/api.js")
let app = getApp();
Page({

    /**
     * 页面的初始数据content: {},
     */
    data: {
        type: '',
        isfollow: 0,
        id: 0,
        uid: 0,
        cover: '',
        nvabarData: {
            showCapsule: 1, //是否显示左上角图标   1表示显示    0表示不显示
            title: '标记详情', //导航栏 中间的标题
        },
        height: app.globalData.height * 2 + 20,
        upa: 1,
        userid: 0,
        intro: '',
        username: '',
    },
    rivertourlist: function(e) {
        var that = this
        wx.navigateTo({
            url: '../rivertourlist/rivertourlist?id=' + e.currentTarget.id + '&type=' + that.data.type + '&markid=' + e.currentTarget.dataset.markid,
        })
    },
    follow: function() {
        var that = this
        wx.getStorage({
            key: 'userinfo',
            success: function(res) {
                var data = {
                    account_id: res.data.id,
                    id: that.data.id,
                    type: that.data.type,
                }
                var url = app.u.H + app.u.river.FOLLOW
                api.requestUrl(data, url).then(res => {
                    if (res.code == 200) {
                        that.setData({
                            isfollow: res.data
                        })
                    }
                })
            },
        })

    },
    save: function(e) {
        wx.showModal({
            title: '保存图片',
            content: '确定要保存吗？',
            showCancel: true,
            success(res) {
                if (res.confirm) {
                    var img = e.currentTarget.dataset.src //获取图片
                    wx.downloadFile({
                        url: img,
                        success(res) {
                            // 只要服务器有响应数据，就会把响应内容写入文件并进入 success 回调，业务需要自行判断是否下载到了想要的内容
                            if (res.statusCode === 200) {
                                // wx.playVoice({
                                //     filePath: res.tempFilePath
                                // })

                                wx.saveImageToPhotosAlbum({
                                    filePath: res.tempFilePath,
                                    success: function(res) {
                                        wx.showToast({
                                            title: '保存图片成功!~',
                                        });

                                    },
                                    fail: function(res) {
                                        wx.showToast({
                                            title: '保存图片失败!~',
                                        });
                                    }
                                })
                            }
                        }
                    })
                } else if (res.cancel) {}
            }
        })

    },
    give: function(e) {
        var that = this
        var data = {
            type: 'mark',
            id: e.currentTarget.id,
            account_id: wx.getStorageSync('userinfo').id
        }
        var url = app.u.H + app.u.river.UPSINDEX

        var dataups = "content.ups"
        var upstateups = "content.upstate"
        api.requestUrl(data, url).then(res => {
            if (res.code == 200) {
                var ups = that.data.content.ups
                that.setData({
                    [dataups]: ups + 1,
                    [upstateups]: 1
                })
            } else {
                api.showError(res.msg)
            }
        })
    },
    comments: function(e) {
        wx.navigateTo({
            url: '../comments/comments?id=' + e.currentTarget.id,
        })
    },
    picture: function(e) {
        var that = this
        wx.navigateTo({
            url: '../picture/picture?id=' + e.currentTarget.dataset.id + '&type=' + that.data.type,
        })
    },
    cruisedetails3: function(e) {
        var that = this
        var src = e.currentTarget.dataset.src; //获取data-src
        var imgList = e.currentTarget.dataset.list; //获取data-list
        //图片预览
        wx.previewImage({
            current: src, // 当前显示图片的http链接
            urls: [imgList] // 需要预览的图片http链接列表
        })
    },
    address: function(e) {
        wx.navigateTo({
            url: '../plat/plat?latitude=' + e.currentTarget.dataset.latitude + '&longitude=' + e.currentTarget.dataset.longitude,
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        console.log('我跳过来了')
        var that = this
        that.setData({
            type: options.type,
            id: options.id,
        })
        wx.getStorage({
            key: 'userinfo',
            success: function(res) {
                that.setData({
                    cover: res.data.cover,
                })
            }
        })
        var data = {
            id: that.data.id,
            account_id: wx.getStorageSync('userinfo').id
        }
        var url = app.u.H + app.u.river.GETMARKINFO
        api.requestUrl(data, url).then(res => {
            if (res.code == 200) {

                that.setData({
                    content: res.data,
                    isfollow: res.data.followstate,
                    userid: res.data.account_id,
                    intro: res.data.intro,
                    username: res.data.username
                })
            }
        })

        wx.showShareMenu({
            // 是否使用带 shareTicket 的转发
            withShareTicket: true
        })
    },
    cruisedetails2: function(e) {
        var that = this
        var src = e.currentTarget.dataset.src; //获取data-src
        var imgList = e.currentTarget.dataset.list; //获取data-list
        //图片预览
        wx.previewImage({
            current: src, // 当前显示图片的http链接
            urls: imgList // 需要预览的图片http链接列表
        })
    },
    comments: function(e) {
        wx.navigateTo({
            url: '../comments/comments?id=' + e.currentTarget.id,
        })
    },
    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {
        var that = this

    },
    copyText(e) {
        console.log(e)
        wx.setClipboardData({
            data: e.currentTarget.dataset.txt,
            success: function(res) {
                wx.getClipboardData({
                    success: function(res) {
                        wx.showToast({
                            title: '复制成功'
                        })
                    }
                })
            }
        })
    },
    onShareAppMessage(res) {
        var that = this
        that.setData({
                uid: wx.getStorageSync('userinfo').id
            })
            // let nickname = wx.getStorageSync('userinfo').nick_name
        if (res.from === 'button') {
            if (that.data.uid == that.data.userid) {
                return {
                    title: that.data.intro,
                    path: '/pages/cruisedetails/cruisedetails?account_id=' + that.data.uid + '&id=' + that.data.id + '&type=' + that.data.type,
                    imageUrl: that.data.content.photos[0],
                    success: function(res) {
                        console.log(res + '2222222222222')
                    }
                }
            } else {
                return {
                    title: '我关注' + that.data.username + ',' + that.data.intro,
                    path: '/pages/cruisedetails/cruisedetails?account_id=' + that.data.uid + '&id=' + that.data.id + '&type=' + that.data.type,
                    imageUrl: that.data.content.photos[0],
                    success: function(res) {
                        console.log(res + '2222222222222')
                    }
                }
            }

        } else {
            if (that.data.uid == that.data.userid) {
                return {
                    title: that.data.intro,
                    path: '/pages/cruisedetails/cruisedetails?account_id=' + that.data.uid + '&id=' + that.data.id + '&type=' + that.data.type,
                    imageUrl: that.data.content.photos[0],
                    // 转发成功的回调函数
                    success: function(res) {
                        // 分享给个人：{errMsg: 'shareAppMessage:ok'}
                        // 分享给群：{errMsg: 'shareAppMessage:ok', shareTickets: Array(1)}
                        /* shareTicket 数组
                         * 每一项是一个 shareTicket(是获取转发目标群信息的票据) ,对应一个转发对象
                         */
                        console.log("成功分享" + "9999999999")
                        var shareTicket = (res.shareTickets && res.shareTickets[0]) || ''

                        wx.getShareInfo({
                            // 把票据带上
                            shareTicket: shareTicket,
                            success: function(res) {
                                console.log("成功分享")
                            }
                        })
                    },
                    fail: function(res) {}
                }
            } else {
                return {
                    title: '我关注' + that.data.username + ',' + that.data.intro,
                    path: '/pages/cruisedetails/cruisedetails?account_id=' + that.data.uid + '&id=' + that.data.id + '&type=' + that.data.type,
                    imageUrl: that.data.content.photos[0],
                    // 转发成功的回调函数
                    success: function(res) {
                        // 分享给个人：{errMsg: 'shareAppMessage:ok'}
                        // 分享给群：{errMsg: 'shareAppMessage:ok', shareTickets: Array(1)}
                        /* shareTicket 数组
                         * 每一项是一个 shareTicket(是获取转发目标群信息的票据) ,对应一个转发对象
                         */
                        console.log("成功分享" + "9999999999")
                        var shareTicket = (res.shareTickets && res.shareTickets[0]) || ''
                        
                        wx.getShareInfo({
                            // 把票据带上
                            shareTicket: shareTicket,
                            success: function(res) {
                                console.log("成功分享")
                            }
                        })
                    },
                    fail: function(res) {}
                }
            }
        }
    }
})