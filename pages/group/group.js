// pages/group/group.js
var util = require("../../utils/util.js")
var api = require("../../utils/api.js")
var QQMapWX = require('../../utils/qqmap/qqmap-wx-jssdk.js');
let app = getApp();
var qqmapsdk;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    type: 1,
    nvabarData: {
      showCapsule: 0, //是否显示左上角图标   1表示显示    0表示不显示
      title: '团队', //导航栏 中间的标题
    },
    // 此页面 页面内容距最顶部的距离
    height: app.globalData.height * 2 + 20,
    uid: 0,
    page: 1,
    province: '',
    city: '',
    latitude: '',
    longitude: '',
    name: '',
    useinput: '',
    place: "请输入团队/团队名称",
    contentjojn: 0,
    account_id: '',
    groupid: 0,
    groupcount: 0,
    Acount: 0,
    acontent: {
      // address: "阳光100后海(长沙市岳麓区)",
      // city: "长沙市",
      // count: 7,
      // id: 18,
      // length: 1228.5,
      // name: "河流守望者",
      // num: 7,
      // patrol_marks: 33,
      // pic: "https://wntest.stw88.com/static/upload/weixin/20190304/8e91c6dfaeec54054a53b47b0b7d4461_y.jpg",
      // province: "湖南省"
    },
    usercount: 0,
    nickname: '',
    sharearray: ['加入团队一起巡，环保路上不孤单', '在巡河宝，组团去巡河。'],
    groupids: {}
  },
  create: function() {
    var data = {
      account_id: wx.getStorageSync('userinfo').id
    }
    var url = app.u.H + app.u.river.CHECKPOINTS
    api.requestUrl(data, url).then(res => {
      if (res.code == 200) {
        wx.navigateTo({
          url: '../create/create?pid=0',
        })
      } else {
        api.showError(res.msg)
      }

    })

  },
  cruisedetails2: function(e) {
    var that = this
    var src = e.currentTarget.dataset.src; //获取data-src
    var imgList = e.currentTarget.dataset.list; //获取data-list
    //图片预览
    wx.previewImage({
      current: src, // 当前显示图片的http链接
      urls: [imgList] // 需要预览的图片http链接列表
    })
  },
  userinput: function(e) {
    var that = this
    that.setData({
      useinput: e.detail.value
    })
  },
  //搜索
  groupsearch: function() {
    var that = this
    var data = {
      city: that.data.city,
      latitude: that.data.latitude,
      longitude: that.data.longitude,
      pages: 1,
      name: that.data.useinput
    }
    var url = app.u.H + app.u.river.GROUPLIST
    api.requestUrl(data, url).then(res => {
      if (res.code == 200) {
        that.setData({
          content: res.data,
          page: 1
        })
      } else {

      }
    })
  },
  //清空
  // empty: function() {
  //     var that = this
  //     that.setData({
  //         useinput: '',
  //         place: ''
  //     })
  // },
  groupof: function(e) {
    var that = this
    let id = e.currentTarget.dataset.id
    var auid = wx.getStorageSync('userinfo').id
    if (that.data.groupid == id) {
      wx.navigateTo({
        url: '../dynamic/dynamic?id=' + id,
      })
    } else {
      console.log(wx.getStorageSync('userinfo').id)
      console.log(e.currentTarget.id)
      if (auid == e.currentTarget.id) {
        wx.navigateTo({
          url: '../dynamic/dynamic?id=' + id,
        })
      } else {
        wx.navigateTo({
          url: '../groupof/groupof?id=' + id,
        })
      }

    }
  },
  groupod: function(e) {
    var that = this
    console.log(e)
    let id = e.currentTarget.dataset.id
    wx.navigateTo({
      url: '../dynamic/dynamic?id=' + id,
    })
  },
  tableCheck: function(e) {
    var that = this
    that.setData({
      type: e.currentTarget.dataset.id
    })
  },
  join: function() {
    wx.navigateTo({
      url: '../addgroup/addgroup',
    })
  },
  search: function() {
    wx.navigateTo({
      url: '../search/search',
    })
  },
  getUserLocation: function() {
    let vm = this;
    wx.getSetting({
      success: (res) => {
        // res.authSetting['scope.userLocation'] == undefined    表示 初始化进入该页面
        // res.authSetting['scope.userLocation'] == false    表示 非初始化进入该页面,且未授权
        // res.authSetting['scope.userLocation'] == true    表示 地理位置授权
        if (res.authSetting['scope.userLocation'] != undefined && res.authSetting['scope.userLocation'] != true) {
          wx.showModal({
            title: '请求授权当前位置',
            content: '需要获取您的地理位置，请确认授权',
            success: function(res) {
              if (res.cancel) {
                wx.showToast({
                  title: '拒绝授权',
                  icon: 'none',
                  duration: 1000
                })
              } else if (res.confirm) {
                wx.openSetting({
                  success: function(dataAu) {
                    if (dataAu.authSetting["scope.userLocation"] == true) {
                      wx.showToast({
                        title: '授权成功',
                        icon: 'success',
                        duration: 1000
                      })
                      //再次授权，调用wx.getLocation的API
                      vm.getLocation();
                    } else {
                      wx.showToast({
                        title: '授权失败',
                        icon: 'none',
                        duration: 1000
                      })
                    }
                  }
                })
              }
            }
          })
        } else if (res.authSetting['scope.userLocation'] == undefined) {
          //调用wx.getLocation的API
          vm.getLocation();
        } else {
          //调用wx.getLocation的API
          vm.getLocation();
        }
      }
    })
  },
  // 微信获得经纬度
  getLocation: function() {
    let vm = this;
    wx.getLocation({
      type: 'wgs84',
      success: function(res) {
        var latitude = res.latitude
        var longitude = res.longitude
        var speed = res.speed
        var accuracy = res.accuracy;
        vm.getLocal(latitude, longitude)
      },
      fail: function(res) {}
    })
  },
  getLocal: function(latitude, longitude) {
    let vm = this;
    qqmapsdk.reverseGeocoder({
      location: {
        latitude: latitude,
        longitude: longitude
      },
      success: function(res) {
        let province = res.result.ad_info.province
        let city = res.result.ad_info.city

        var usercity = wx.getStorageSync('usercity')
        var userparent = wx.getStorageSync('userparent')
        if (usercity == '' && userparent == '') {
          vm.setData({
            province: province,
            city: city,
            latitude: latitude,
            longitude: longitude,
          })
          vm.getindexinfo(province, city, latitude, longitude)
        } else {
          vm.setData({
            province: province,
            city: city,
            latitude: latitude,
            longitude: longitude,
          })
          vm.getindexinfo(userparent, usercity, latitude, longitude)
        }
      },
      fail: function(res) {},
      complete: function(res) {
        // console.log(res);
      }
    });
  },
  getindexinfo: function(province, city, latitude, longitude) {
    let vm = this;
    //查询数据
    var data = {
      city: city,
      latitude: latitude,
      longitude: longitude,
      pages: 1,
      name: vm.data.useinput
    }
    var url = app.u.H + app.u.river.GROUPLIST
    api.requestUrl(data, url).then(res => {
      if (res.code == 200) {
        vm.setData({
          content: res.data,
          city: city,
        })
      } else {

      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    let grouplist = wx.getStorageSync('grouplist') || ''
    if (grouplist) {
      this.setData({
        content: grouplist
      })
    }
  },
  query(e) {
    var that = this
    var data = {
      id: e,
    }
    var url = app.u.H + app.u.river.USERGROUPINFO
    api.requestUrl(data, url).then(res => {
      if (res.code == 200) {
        that.setData({
          acontent: res.data,
        })
      } else {
        that.setData({
          acontent: {},
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    var that = this
    // var usercity = wx.getStorageSync('usercity')
    // var userparent = wx.getStorageSync('userparent')
    // console.log(usercity)
    // console.log(userparent)
    // if (usercity == '' && userparent == '') {

    // } else {
    //     that.getindexinfo(userparent, usercity)
    // }
    wx.getStorage({
      key: 'userinfo',
      success(res) {
        that.setData({
          account_id: res.data.id,
          groupid: res.data.group_id,
          nickname: res.data.nick_name
        })
        if (res.data.group_id) {
          that.query(res.data.group_id)
        } else {
          that.setData({
            acontent: {}
          })
        }
        let data = {
          account_id: res.data.id
        }
        let url = app.u.H + app.u.river.GETGROUPID
        api.requestUrl(data, url).then(res => {
          that.setData({
            groupids: res.data,
          })
        })
      }
    })
    wx.getStorage({
      key: 'usercount',
      success(res) {
        that.setData({
          usercount: res.data
        })
      }
    })

    let url = app.u.H + app.u.river.GETACCOUNT
    api.requestUrl('', url).then(res => {
      that.setData({
        groupcount: res.data.groupcount,
        Acount: res.data.Acount
      })
    })


    qqmapsdk = new QQMapWX({
      key: 'TTWBZ-6L56U-W55V3-BJZJ6-KGGYF-O2BRB'
    })
    wx.getLocation({
      type: 'gcj02',
      success: function(res) {
        that.getUserLocation()
        that.getLocal(res.laitude, res.longitude)
      },
    })
    // if (that.data.contentjojn == 0) {} else {

    // }
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    let that = this
    let newpage = that.data.page + 1
    var data = {
      city: that.data.city,
      latitude: that.data.latitude,
      longitude: that.data.longitude,
      pages: newpage,
      name: that.data.useinput
    }
    var url = app.u.H + app.u.river.GROUPLIST
    api.requestUrl(data, url).then(res => {
      if (res.code == 200) {

        if (res.data.length > 0) {
          that.setData({
            content: that.data.content.concat(res.data),
            page: newpage
          })
        }
      } else {
        api.showError(res.msg)
      }
    })
  },
  onShareAppMessage: function(res) {
    var that = this

    let uid = wx.getStorageSync('userinfo').id
    let nickname = wx.getStorageSync('userinfo').nick_name
    var random = Math.floor(Math.random() * 2);
    if (res.from === 'button') {
      // 来自页面内转发按钮
    }

    return {
      title: "加入团队一起巡，环保路上不孤单",
      path: "pages/group/group?account_id=" + uid,
      // imageUrl: '../res/wxshare.jpg',
    }
  },
})