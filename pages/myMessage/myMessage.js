// pages/myMessage/myMessage.js
var util = require("../../utils/util.js")
var api = require("../../utils/api.js")
let app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        nvabarData: {
            showCapsule: 1, //是否显示左上角图标   1表示显示    0表示不显示
            title: '消息', //导航栏 中间的标题
        },
        type: 1,
        height: app.globalData.height * 2 + 20,
        list: [],
        list1: [],
        list2: [],
        messgeType: 'ups',
        pages: 1,
    },
    tableCheck: function(e) {
        var that = this;
        const id = e.currentTarget.id;
        let type = 'ups';
        if (id == 1) {
            type = 'ups';
        } else if (id == 2) {
            type = 'follow';
        } else if (id == 3) {
            type = 'comments';
        }
        that.getList(type);
        that.setData({
            type: e.currentTarget.id,
            messgeType: type
        })
    },
    cruisedetails: function(e) {
        var that = this
        wx.navigateTo({
            url: '../cruisedetails/cruisedetails?id=' + e.currentTarget.dataset.id + "&type=mark",
        })
    },
    getList(type) {
        let that = this;
        var data = {
            account_id: wx.getStorageSync('userinfo').id,
            type: type,
            pages: 1,
        }
        var url = app.u.H + app.u.river.GETMSG
        api.requestUrl(data, url).then(res => {
            res.data.map(item => {
                if (item.contents.indexOf(item.user) > -1) {
                    item.contents = item.contents.replace(item.user, "");
                }
                return item;
            });
            if (type == 'ups') {
                that.setData({
                    list: res.data,
                    pages: 1,
                })
            } else if (type == 'follow') {
                that.setData({
                    list1: res.data,
                    pages: 1,
                })
            } else if (type == 'comments') {
                that.setData({
                    list2: res.data,
                    pages: 1,
                })
            }
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        this.getList("ups");
        this.getList("comments");
        this.getList("follow");
        var message = 0
        wx.setStorageSync('message', message)
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {},

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {
        let that = this;
        var data = {
            account_id: wx.getStorageSync('userinfo').id,
            type: this.data.messgeType,
            pages: that.data.pages + 1,
        }
        var url = app.u.H + app.u.river.GETMSG
        api.requestUrl(data, url).then(res => {
            if (this.data.messgeType = 'ups') {
                that.setData({
                    list: that.data.list.concat(res.data),
                    pages: that.data.pages + 1,
                })
            } else if (this.data.messgeType = 'follow') {
                that.setData({
                    list1: that.data.list1.concat(res.data),
                    pages: that.data.pages + 1,
                })
            } else if (this.data.messgeType = 'comments') {
                that.setData({
                    list2: that.data.list2.concat(res.data),
                    pages: that.data.pages + 1,
                })
            }
        })
    },

})