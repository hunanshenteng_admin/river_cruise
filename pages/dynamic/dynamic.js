// pages/dynamic/dynamic.js
var util = require("../../utils/util.js")
var api = require("../../utils/api.js")
let app = getApp();
let a = false
Page({

    /**
     * 页面的初始数据
     */
    data: {
        nvabarData: {
            showCapsule: 1, //是否显示左上角图标   1表示显示    0表示不显示
            title: '团队详情', //导航栏 中间的标题
        },
        // 此页面 页面内容距最顶部的距离
        height: app.globalData.height * 2 + 20,
        type: 1,
        userid: 0,
        id: 0,
        userInfo: {
            content: 0
        },
        address: '',
        intro: '',
        name: '',
        pic: '',
        contentdata: {},
        page: 2,
        newmarks: [],
        share_query: '',
    },
    tableCheck: function(e) {
        var that = this
        that.setData({
            type: e.currentTarget.id
        })
    },
    paimin: function(e) {
        var that = this
        wx.navigateTo({
            url: '../ranking/ranking?type=1&id=' + e.currentTarget.dataset.id,
        })
    },
    paimin2: function(e) {
        var that = this
        wx.navigateTo({
            url: '../ranking/ranking?type=2&id=' + e.currentTarget.dataset.id,
        })
    },
    cruisedetails: function(e) {
        var that = this
        wx.navigateTo({
            url: '../cruisedetails/cruisedetails?id=' + e.currentTarget.dataset.id + '&type=mark',
        })
    },
    cruisedetails2: function(e) {
        a = true
        var that = this
        var src = e.currentTarget.dataset.src; //获取data-src
        var imgList = e.currentTarget.dataset.list; //获取data-list
        //图片预览
        wx.previewImage({
            current: src, // 当前显示图片的http链接
            urls: imgList // 需要预览的图片http链接列表
        })
    },
    cruisedetails3: function(e) {
        a = true
        var that = this
        var src = e.currentTarget.dataset.src; //获取data-src
        var imgList = e.currentTarget.dataset.list; //获取data-list
        //图片预览
        wx.previewImage({
            current: src, // 当前显示图片的http链接
            urls: [imgList] // 需要预览的图片http链接列表
        })
    },
    give: function(e) {
        var that = this
        var data = {
            type: 'mark',
            id: e.currentTarget.id,
            account_id: wx.getStorageSync('userinfo').id
        }
        var url = app.u.H + app.u.river.UPSINDEX
        var dataups = 'newmarks[' + e.currentTarget.dataset.index + '].ups'
        var userups = 'newmarks[' + e.currentTarget.dataset.index + '].upstate'
        api.requestUrl(data, url).then(res => {
            if (res.code == 200) {
                var ups = e.currentTarget.dataset.ups
                that.setData({
                    [dataups]: ups + 1,
                    [userups]: 1
                })
                api.showError(res.msg)
            } else {
                api.showError(res.msg)
            }
        })
    },
    bjoin: function(e) {
        var that = this
        console.log(e)
        wx.navigateTo({
          url: '../grfeditor/grfeditor?id=' + e.currentTarget.dataset.id + '&address=' + that.data.address + '&intro=' + that.data.intro + '&name=' + that.data.name + '&pic=' + that.data.pic + '&longitude=' + e.currentTarget.dataset.longitude + '&latitude=' + e.currentTarget.dataset.latitude + '&province=' + that.data.province + '&city=' + that.data.city,
        })

    },
    //退出群组
    tjoin: function(e) {
        var that = this
            // that.setData({
            //     userid: e.currentTarget.dataset.id,
            // })
        var data = {
            id: that.data.id,
            account_id: wx.getStorageSync('userinfo').id
        }
        var url = app.u.H + app.u.river.EXITGROUP
        wx.showModal({
            title: '退出团队',
            content: '你确定要退出团队吗？',
            success: function(res) {
                if (res.confirm) {
                    console.log('用户点击确定')
                    api.requestUrl(data, url).then(res => {
                        wx.setStorageSync('userInfo', that.data.userInfo)
                        if (res.code == 200) {
                            // setTimeout(function() {
                            //     api.showError(res.msg)
                            //     wx.switchTab({
                            //         url: '../newpage/newpage'
                            //     })
                            // }, 1000)
                            setTimeout(function() {
                                api.showError(res.msg)
                                wx.switchTab({
                                    url: '../group/group'
                                })
                            }, 1000)
                            wx.setStorage({
                                key: 'userinfo',
                                data: res.data,
                            })
                            api.showError(res.msg)
                        } else {
                            api.showError(res.msg)
                        }
                    })
                } else {
                    console.log('用户点击取消')
                }
            }
        })

    },
    //解散团队
    dissolution: function(e) {
        var that = this
            // that.setData({
            //     userid: e.currentTarget.dataset.id,
            // })
        var data = {
            id: that.data.id,
            account_id: wx.getStorageSync('userinfo').id
        }
        var url = app.u.H + app.u.river.EXITGROUP
        wx.showModal({
            title: '解散团队',
            content: '你确定要解散团队吗？',
            success: function(res) {
                if (res.confirm) {
                    console.log('用户点击确定')
                    api.requestUrl(data, url).then(res => {
                        console.log(res)
                        wx.setStorageSync('userInfo', that.data.userInfo)
                        if (res.code == 200) {
                            // setTimeout(function() {
                            //     api.showError(res.msg)
                            //     wx.switchTab({
                            //         url: '../newpage/newpage'
                            //     })
                            // }, 1000)
                            wx.setStorage({
                                key: 'userinfo',
                                data: res.data,
                            })
                            setTimeout(function() {
                                api.showError(res.msg)
                                wx.switchTab({
                                    url: '../group/group'
                                })
                            }, 1000)

                            api.showError(res.msg)
                        } else {
                            api.showError(res.msg)
                        }
                    })
                } else {
                    console.log('用户点击取消')
                }
            }
        })

    },
    comments: function(e) {
        wx.navigateTo({
            url: '../comments/comments?id=' + e.currentTarget.id,
        })
    },
    transfer: function(e) {
        wx.navigateTo({
            url: '../grftransfer/grftransfer?id=' + e.currentTarget.dataset.id,
        })
    },
    create: function(e) {
        wx.navigateTo({
            url: '../create/create?pid=' + e.currentTarget.dataset.id,
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        a = false
        var that = this
        that.setData({
            id: options.id,
        })
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {
        if (a) {
            return
        }
        var that = this
        wx.getStorage({
            key: 'userinfo',
            success(res) {
                that.setData({
                    account_id: res.data.id
                })
                let data = { account_id: res.data.id}
				let url = app.u.H + app.u.river.GETGROUPID
				api.requestUrl(data, url).then(res => {
                      let group_id = res.data.join(',')
                      console.log(group_id)
                      console.log('index',group_id.indexOf(that.data.id))
                      if(group_id.indexOf(that.data.id)>-1){
                        var data = {
                            id: that.data.id,
                            account_id: wx.getStorageSync('userinfo').id
                        }
                        // var data2 = {
                        //     id: that.data.id,
                        //     account_id: wx.getStorageSync('userinfo').id,
                        //     pages: that.data.page
                        // }
    
                    var url = app.u.H + app.u.river.GROUPINFO
                    api.requestUrl(data, url).then(res => {
                        if (res.code == 200) {
                            console.log(res)
                            that.setData({
                                content: res.data,
                                address: res.data.address,
                                intro: res.data.intro,
                                name: res.data.name,
                                pic: res.data.pic,
                                newmarks: res.data.newmarks,
                                province:res.data.province,
                                city:res.data.city
                            })
                            let data = { account_id: res.data.id }
                            let url = app.u.H + app.u.river.GETGROUPID
                            api.requestUrl(data, url).then(res => {
                                
                                let groupids = groupids;
                                if(res.code==200){
									if (wx.getStorageSync('userinfo').group_id == that.data.id || group_id.indexOf(that.data.id)>-1) {
    
                                } else {
                                    if (wx.getStorageSync('userinfo').id == res.data.account_id) {
    
                                    } else {
                                        wx.showLoading({
                                            title: '跳转加入页面中··',
                                        })
                                        setTimeout(function () {
                                            getCurrentPages().pop()
                                            wx.hideLoading()
                                            wx.redirectTo({
                                                url: '/pages/groupof/groupof?id=' + that.data.id,
                                            })
                                        }, 1000)
                                    }
                                }
                                }
                            })
                            
                        }
                    })
                      }else{
                        wx.showLoading({
                            title: '跳转加入页面中··',
                        })
                        setTimeout(function () {
                            getCurrentPages().pop()
                            wx.hideLoading()
                            wx.redirectTo({
                                url: '/pages/groupof/groupof?id=' + that.data.id,
                            })
                        }, 1000)
                       
                      }  
                        
				})
                
            },
            fail(res) {
                wx.navigateTo({
                    url: '../sq/sq'
                })
            }
        })



        // if (that.data.share_query) {
        // 	wx.showLoading({
        // 		title: '加载中',
        // 	})
        // 	if (wx.getStorageSync('userinfo').group_id == that.data.id) {

        // 	} else {
        // 		setTimeout(function () {
        // 			wx.hideLoading()
        // 			wx.redirectTo({
        // 				url: '/pages/groupof/groupof?id=' + that.data.id,
        // 			})
        // 		}, 1000)
        // 	}
        // }
    },
    onReachBottom: function() {
        let that = this
        let newpage = that.data.page + 1
        var data = {
            id: that.data.id,
            account_id: wx.getStorageSync('userinfo').id,
            pages: newpage
        }
        var url = app.u.H + app.u.river.GETMOREMARKS
        api.requestUrl(data, url).then(res => {
            if (res.code == 200) {

                that.setData({
                    newmarks: that.data.newmarks.concat(res.data),
                    page: newpage
                })
            } else {
                api.showError(res.msg)
            }
        })
    },
    onShareAppMessage: function(res) {
        var that = this
        let uid = wx.getStorageSync('userinfo').id
        let nickname = wx.getStorageSync('userinfo').nick_name
        if (res.from === 'button' && res.target.id) {
            // 来自页面内转发按钮
            if (that.data.newmarks[res.target.dataset.index].photos) {
                return {
                    // title: nickname + " 邀请你加入" + that.data.name + "团队，一起去巡河。",
                    title: that.data.newmarks[res.target.dataset.index].intro,
                    path: "/pages/cruisedetails/cruisedetails?account_id=" + uid + "&id=" + that.data.id + "&type=mark",
                    imageUrl: that.data.newmarks[res.target.dataset.index].photos[0],
                    // imageUrl: '../res/wxshare.jpg',
                }
            } else {
                return {
                    title: that.data.newmarks[res.target.dataset.index].intro,
                    path: "/pages/cruisedetails/cruisedetails?account_id=" + uid + "&id=" + that.data.id + "&type=mark",
                    // imageUrl: that.data.newmarks[res.target.dataset.index].photos[0],
                    imageUrl: '../res/wxshare.jpg',
                }
            }

        } else {
            return {
                title: nickname + " 邀请你加入" + that.data.name + "团队，一起去巡河。",
                path: "/pages/dynamic/dynamic?account_id=" + uid + "&id=" + that.data.id,
                // imageUrl: '../res/wxshare.jpg',
            }
        }
    },
})