// pages/article/article.js
var util = require("../../utils/util.js")
var api = require("../../utils/api.js")
let app = getApp();
var WxParse = require('../../utils/wxParse/wxParse.js');
Page({

    /**
     * 页面的初始数据
     */
    data: {
        content: {},
        resBaseUrl: '',
        nvabarData: {
            showCapsule: 1, //是否显示左上角图标   1表示显示    0表示不显示
            title: '帮助', //导航栏 中间的标题
        },
        height: app.globalData.height * 2 + 20,
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        var that = this
        that.setData({
            resBaseUrl: app.globalData.resBaseUrl,
        })
        var data = {
            art_id: options.id
        }
        if (options.id == 118) {
            that.setData({
                nvabarData: {
                    showCapsule: 1,
                    title: '巡河宝简介',
                },
            })
        } else {
            that.setData({
                nvabarData: {
                    showCapsule: 1,
                    title: '帮助',
                },
            })
        }
        console.log(options.id)
        var url = app.u.H + app.u.river.ACTICLE
        api.requestUrl(data, url).then(res => {
            WxParse.wxParse('content1', 'html', res.data.content, this, 5);
            that.setData({
                content: res.data,
            })
        })
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },
    onShareAppMessage(res) {

    }
})