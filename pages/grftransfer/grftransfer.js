// pages/grftransfer/grftransfer.js
var util = require("../../utils/util.js")
var api = require("../../utils/api.js")
let app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        nvabarData: {
            showCapsule: 1, //是否显示左上角图标   1表示显示    0表示不显示
            title: '转让队长', //导航栏 中间的标题
        },
        // 此页面 页面内容距最顶部的距离
        height: app.globalData.height * 2 + 20,
        id: 0,
        content: {},
        name: '',
        tag: 0,
    },
    userinput: function(e) {
        this.setData({
            name: e.detail.value
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        var that = this
        that.setData({
            id: options.id
        })
        var data = {
            id: options.id
        }
        var url = app.u.H + app.u.river.GETGROUPUSER
        api.requestUrl(data, url).then(res => {
            if (res.code == 200) {
                that.setData({
                    content: res.data
                })
            } else {
                api.showError(res.msg)
            }

        })
    },
    search: function() {
        var that = this
        var data = {
            id: that.data.id,
            name: that.data.name
        }
        var url = app.u.H + app.u.river.GETGROUPUSER
        api.requestUrl(data, url).then(res => {
            if (res.code == 200) {
                if (res.data.length == 0) {
                    api.showError('未找到此成员')
                    that.setData({
                        content: res.data,
                        tag: 0,
                    })
                } else {
                    if (that.data.name == '') {
                        that.setData({
                            content: res.data,
                            tag: 0,
                        })
                    } else {
                        that.setData({
                            content: res.data,
                            tag: 1,
                        })
                    }
                }

            } else {
                api.showError(res.msg)
            }

        })
    },
    transfer: function(e) {
        var that = this
        wx.showModal({
            title: '转让队长',
            content: '你确定！要转让队长吗？',
            success: function(res) {
                if (res.confirm) { //这里是点击了确定以后
                    console.log('用户点击确定')
                    if (wx.getStorageSync('userinfo').id == e.currentTarget.dataset.id) {
                        api.showError('你已经是队长')
                    } else {
                        var data = {
                            id: that.data.id,
                            account_id: wx.getStorageSync('userinfo').id,
                            uid: e.currentTarget.dataset.id
                        }
                        var url = app.u.H + app.u.river.UPDATEGROUPUSER
                        api.requestUrl(data, url).then(res => {
                            if (res.code == 200) {
                                setTimeout(function() {
                                    api.showError(res.msg)
                                    wx.redirectTo({
                                        url: '../dynamic/dynamic?id=' + that.data.id,
                                    })
                                }, 1000)

                            } else {
                                api.showError(res.msg)
                            }

                        })
                    }
                } else { //这里是点击了取消以后
                    console.log('用户点击取消')
                }
            }
        })
    },
    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },
})