var Api = require("./api.js")
var Config = {}
Config.session_id = "None"; //用户默认微信id

Config.getUserInfoAndWXOpenId = function (callback) {
  wx.login({
    success: function (res) {
      function wx_openid_callback(wx_res) {
        console.log(res)
        wx.getUserInfo({
          success: function (res) {
            callback(wx_res.data.wx_openid, res.rawData)
          }
        })
      }
      var params = {
        code: res.code
      }
      wx.hideLoading();
      Api.userLogin(params, wx_openid_callback)

    }
  })
}
Config.Error = {

}


/* 随机数 */
Config.randomString = function() {
    var chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678'; /****默认去掉了容易混淆的字符oOLl,9gq,Vv,Uu,I1****/
    var maxPos = chars.length;
    var pwd = '';
    for (var i = 0; i < 32; i++) {
      pwd += chars.charAt(Math.floor(Math.random() * maxPos));
    }
    return pwd;
  },
  /* 获取prepay_id */
  Config.getXMLNodeValue = function(node_name, xml) {
    var tmp = xml.split("<" + node_name + ">")
    var _tmp = tmp[1].split("</" + node_name + ">")
    return _tmp[0]
  },

  /* 时间戳产生函数   */
  Config.createTimeStamp = function() {
    return parseInt(new Date().getTime() / 1000) + ''
  }

module.exports = Config